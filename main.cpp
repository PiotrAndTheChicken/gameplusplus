#include <SFML/Graphics.hpp>
#include <list>

class GameActor
{
    public:
    void Draw(sf::RenderWindow* window)
    {
        if(CommonDraw != NULL)
        {
            window->draw(*CommonDraw);
        }
    }
    void AssignShape(sf::RectangleShape* shape)
    {
        this->CommonDraw = shape;
        this->CommonTrans = shape;
    };
    void AssignSprite(sf::Sprite* sprite)
    {
        this->CommonDraw = sprite;
        this->CommonTrans = sprite;
    };
    
    sf::Vector2f getPosition()
    {
        return CommonTrans->getPosition();
    };
    void setPosition(sf::Vector2f& NewPosition)
    {
        CommonTrans->setPosition(NewPosition);
    };

    private:
    //sf::RectangleShape* shape = NULL;
    //sf::Sprite* sprite = NULL;;
    sf::Transformable* CommonTrans = NULL;
    sf::Drawable* CommonDraw = NULL;
};

class Command
{
    public:
        virtual ~Command(){}
        virtual void execute(GameActor& actor) = 0;
};

#define MOVE_BY 10

class MoveUpCommand : public Command
{
    public:
        virtual void execute(GameActor& actor) { MoveUp(actor); }
    private:
    void MoveUp(GameActor& actor)
    {
        auto pos = actor.getPosition();
        pos.y-= MOVE_BY;
        actor.setPosition(pos);
    }
};

class MoveDownCommand : public Command
{
    public:
        virtual void execute(GameActor& actor) { MoveDown(actor); }
    private:
    void MoveDown(GameActor& actor)
    {
        auto pos = actor.getPosition();
        pos.y+= MOVE_BY;
        actor.setPosition(pos);
    }
};

class MoveRightCommand : public Command
{
    public:
        virtual void execute(GameActor& actor) { MoveRight(actor); }
    private:
    void MoveRight(GameActor& actor)
    {
        auto pos = actor.getPosition();
        pos.x+= MOVE_BY;
        actor.setPosition(pos);
    }
};

class MoveLeftCommand : public Command
{
    public:
        virtual void execute(GameActor& actor) { MoveLeft(actor); }
    private:
    void MoveLeft(GameActor& actor)
    {
        auto pos = actor.getPosition();
        pos.x-= MOVE_BY;
        actor.setPosition(pos);
    }
};

class DefaultCommand : public Command
{
    public:
        virtual void execute(GameActor& actor) { DoNothing(actor); }
    private:
    void DoNothing(GameActor& actor)
    {
        
    }
};


/*******************************************/
class InputHandler
{
public:
  Command* handleInput(sf::Event event);
  //InputHandler(){};

  // Methods to bind commands...
  void BindUp(Command* Up){buttonUp = Up;};
  void BindDown(Command* Down){buttonDown = Down;};
  void BindLeft(Command* Left){buttonLeft = Left;};
  void BindRight(Command* Right){buttonRight = Right;};

  

private:
  Command* buttonUp;
  Command* buttonDown;
  Command* buttonLeft;
  Command* buttonRight;
  DefaultCommand Default;
};

Command* InputHandler::handleInput(sf::Event event)
{
  if (event.key.code == sf::Keyboard::Up) return buttonUp;
  else if (event.key.code == sf::Keyboard::Down) return buttonDown;
  else if (event.key.code == sf::Keyboard::Left) return buttonLeft;
  else if (event.key.code == sf::Keyboard::Right) return buttonRight;

  return NULL;
}

/************************************/


int main()
{
    sf::RenderWindow window(sf::VideoMode(500, 500), "GamePlusPlus");
    sf::CircleShape shape1(100.f);
    sf::Vector2f size2(100,100); 
    sf::Vector2f size3(50,200); 
    sf::RectangleShape shape2(size2);
    sf::RectangleShape shape3(size3);
    shape3.setFillColor(sf::Color::Blue);
    shape1.setFillColor(sf::Color::Green);
    sf::Vector2f pos2(0.0,0.0);

    sf::Texture texture;
    // load a 32x32 rectangle that starts at (10, 10)
    if (!texture.loadFromFile("ball_sprite.png"))//, sf::IntRect(10, 10, 120, 120)))
    {
        // error...
    }

    sf::Sprite sprite;
    sprite.setTexture(texture);
    sprite.setColor(sf::Color(0, 255, 0)); // green
    sprite.setPosition(sf::Vector2f(200.0,200.0));


    GameActor SquareGuy;
    GameActor SquareGuy2;
    GameActor BallGuy;
    InputHandler inputHandler;
    inputHandler.BindRight(new MoveRightCommand);
    inputHandler.BindLeft(new MoveLeftCommand);
    inputHandler.BindUp(new MoveUpCommand);
    inputHandler.BindDown(new MoveDownCommand);

    SquareGuy.AssignShape(&shape3);
    shape2.setPosition(size2);
    SquareGuy2.AssignShape(&shape2);
    BallGuy.AssignSprite(&sprite);

    std::list<GameActor> ActorsList = {SquareGuy, SquareGuy2, BallGuy};
    printf("Hello World\n");

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch(event.type)
            {
                case sf::Event::Closed:
                {
                    window.close();
                    break;
                }
                case sf::Event::MouseButtonPressed:
                {
                    sf::Color RandomColor(rand());
                    if(event.mouseButton.button == sf::Mouse::Button::Left)
                    {                        
                        shape1.setFillColor(RandomColor);
                    }
                    else if(event.mouseButton.button == sf::Mouse::Button::Right)
                    {                       
                        shape2.setFillColor(RandomColor);
                    }
                    break;
                }
                case sf::Event::KeyPressed:
                {
                    printf("Key Press\n");
                    Command* command = inputHandler.handleInput(event);
                    if (command)
                    {
                        window.clear();
                        for (auto actor: ActorsList)
                        {
                            command->execute(actor);
                            actor.Draw(&window);
                        }
                        /*command->execute(SquareGuy);
                        command->execute(SquareGuy2);
                        command->execute(BallGuy);*/
                        
                        window.display();
                    }
                }
                deafult:
                    break;
            }                
        }
    }

    return 0;
}